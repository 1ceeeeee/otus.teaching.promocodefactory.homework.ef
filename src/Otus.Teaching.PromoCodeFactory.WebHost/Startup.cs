using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Base;
using Otus.Teaching.PromoCodeFactory.WebHost.Extensions;
using Otus.Teaching.PromoCodeFactory.WebHost.Settings;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class Startup
    {
        public Startup( IConfiguration configuration )
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices( IServiceCollection services )
        {
            services.AddControllers( );
            services.ConfigureContext(
                Configuration.Get<ApplicationSettings>( ).ConnectionString );
            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            services.AddScoped<ICustomerRepository, CustomerRepository>( );
            services.AddScoped<IPreferenceRepository, PreferenceRepository>( );
            services.AddScoped<IEmployeeRepository, EmployeeRepository>( );
            services.AddOpenApiDocument( options =>
            {
                options.Title = "PromoCode Factory API Doc";
                options.Version = "1.0";
            } );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app,
            IWebHostEnvironment env,
            DataContext context )
        {
            app.MigrateAndSeed( context );
            
            if( env.IsDevelopment( ) )
            {
                app.UseDeveloperExceptionPage( );
            }
            else
            {
                app.UseHsts( );
            }

            app.UseOpenApi( );
            app.UseSwaggerUi3( x => { x.DocExpansion = "list"; } );

            app.UseHttpsRedirection( );

            app.UseRouting( );

            app.UseEndpoints( endpoints => { endpoints.MapControllers( ); } );
        }
    }
}