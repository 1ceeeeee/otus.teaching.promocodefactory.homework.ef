﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Customer
{
    public class CustomerResponse
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<PreferenceResponse> Preferences { get; set; }
        public List<PromoCodeShortResponse> PromoCodes { get; set; }

        public CustomerResponse( )
        {
        }

        //TODO добавить автомаппер
        public CustomerResponse( Core.Domain.PromoCodeManagement.Customer customer )
        {
            Id = customer.Id;
            Email = customer.Email;
            FirstName = customer.FirstName;
            LastName = customer.LastName;
            Preferences = customer.Preferences.Select(x => new PreferenceResponse()
            {
                Id = x.PreferenceId,
                Name = x.Preference.Name
            }).ToList();
            PromoCodes = customer.Promocodes.Select( x => new PromoCodeShortResponse( )
            {
                Id = x.Id,
                Code = x.Code,
                PartnerName = x.PartnerName,
                ServiceInfo = x.ServiceInfo,
                BeginDate = x.BeginDate.ToShortDateString(),
                EndDate = x.EndDate.ToShortDateString()
            } ).ToList( );
        }
    }
}