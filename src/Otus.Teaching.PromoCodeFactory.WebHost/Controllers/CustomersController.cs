﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Customer;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route( "api/v1/[controller]" )]
    public class CustomersController
        : ControllerBase
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(
            ICustomerRepository customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить данные всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync( )
        {
            var customers = await _customerRepository.GetAllAsync( );

            var response = customers.Select( x => new CustomerShortResponse( )
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            } ).ToList( );

            return Ok( response );
        }

        /// <summary>
        /// Получить данные клиента по ИД
        /// </summary>
        /// <param name="id">ИД клиента/param>
        /// <returns></returns>
        [HttpGet( "{id}" )]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync( Guid id )
        {
            var customer = await _customerRepository.GetByIdAsync( id );

            var response = new CustomerResponse( customer );

            return Ok( response );
        }

        /// <summary>
        /// Добавить данные о клиенте
        /// </summary>
        /// <param name="request">Данные клиента</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync( CreateOrEditCustomerRequest request )
        {
            var preferences = await _preferenceRepository.GetByIdsAsync( request.PreferenceIds );

            //TODO добавить автомаппер
            var customer = new Customer( )
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
            };

            if( preferences != null )
                customer.Preferences = preferences.Select( x => new CustomerPreference( )
                {
                    CustomerId = customer.Id,
                    PreferenceId = x.Id
                } ).ToList( );

            await _customerRepository.AddAsync( customer );

            return Created( string.Empty, new { id = customer.Id } );
        }

        /// <summary>
        /// Отредактировать данные о клиенте
        /// </summary>
        /// <param name="id">ИД клиента</param>
        /// <param name="request">Данные клиента</param>
        /// <returns></returns>
        [HttpPut( "{id}" )]
        public async Task<IActionResult> EditCustomersAsync(
            Guid id,
            CreateOrEditCustomerRequest request )
        {
            var customer = await _customerRepository.GetByIdAsync( id );

            if( customer == null )
                return NotFound( );

            var preferences = await _preferenceRepository.GetByIdsAsync( request.PreferenceIds );

            //TODO добавить автомаппер
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;

            if( preferences != null )
            {
                customer.Preferences.Clear( );
                customer.Preferences = preferences.Select( x => new CustomerPreference( )
                {
                    CustomerId = customer.Id,
                    PreferenceId = x.Id
                } ).ToList( );
            }

            await _customerRepository.UpdateAsync( customer );

            return Ok( );
        }

        /// <summary>
        /// Удалить данные о клиенте по ИД
        /// </summary>
        /// <param name="id">ИД клиента</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer( Guid id )
        {
            var customer = await _customerRepository.GetByIdAsync( id );

            if( customer == null )
                return NotFound( );

            await _customerRepository.DeleteAsync( customer );

            return Ok( );
        }
    }
}