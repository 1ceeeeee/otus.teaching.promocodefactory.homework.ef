﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route( "api/v1/[controller]" )]
    public class PromoСodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly IPreferenceRepository _preferenceRepository;

        public PromoСodesController(
            IRepository<PromoCode> promoCodesRepository,
            ICustomerRepository customerRepository,
            IPreferenceRepository preferenceRepository)
        {
            _promoCodesRepository = promoCodesRepository;
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync( )
        {
            var preferences = await _promoCodesRepository.GetAllAsync( );

            var response = preferences.Select( x => new PromoCodeShortResponse( )
            {
                Id = x.Id,
                Code = x.Code,
                PartnerName = x.PartnerName,
                ServiceInfo = x.ServiceInfo,
                BeginDate = x.BeginDate.ToShortDateString( ), // default, поле отсутствует в модели
                EndDate = x.EndDate.ToShortDateString( )// default, поле отсутствует в модели
            } ).ToList( );

            return Ok( response );
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync( GivePromoCodeRequest request )
        {
            var preference = await _preferenceRepository.GetByNameAsync( request.Preference );
            if( preference == null )
                return NotFound( $"указаного предпочтения '{request.Preference}' не найдено" );
            
            var customers = await _customerRepository.GetByPreferenceNameAsync( request.Preference );
            var toAdd = new List<PromoCode>( customers.Count );
            
            foreach( var customer in customers )
                toAdd.Add(
                    new PromoCode( )
                    {
                        Code = request.PromoCode,
                        PreferenceId = preference.Id,
                        PartnerName = request.PartnerName,
                        ServiceInfo = request.ServiceInfo,
                        CustomerId = customer.Id
                    } );

            await _promoCodesRepository.AddRangeAsync( toAdd );

            return Ok( );
        }
    }
}