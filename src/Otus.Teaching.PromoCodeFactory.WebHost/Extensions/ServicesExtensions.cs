using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Extensions
{
    public static class ServicesExtensions
    {
        public static IServiceCollection ConfigureContext(
            this IServiceCollection @this,
            string connectionString )
        {
            @this.AddDbContext<DataContext>(
                optionsBuilder
                    => optionsBuilder
                        .UseSqlite( connectionString )
                        .UseLazyLoadingProxies( ) );
            @this.AddScoped<DbContext, DataContext>( );

            return @this;
        }
    }
}