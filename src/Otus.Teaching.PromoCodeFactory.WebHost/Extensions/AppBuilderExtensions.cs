using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Extensions
{
    public static class AppBuilderExtensions
    {
        public static IApplicationBuilder MigrateAndSeed(
            this IApplicationBuilder @this,
            DataContext context )
        {
            var hasPendingMigrations = context.Database.GetPendingMigrations( ).Any( );
            switch( hasPendingMigrations )
            {
                case true when context.Database.GetAppliedMigrations( ).Count( ) <= 1:
                    context.Database.EnsureDeleted( );
                    context.Database.Migrate();

                    context.Employees.AddRange( FakeDataFactory.Employees );
                    context.Preferences.AddRange( FakeDataFactory.Preferences );
                    context.Customers.AddRange( FakeDataFactory.Customers );

                    context.SaveChanges( );
                    break;
                case true:
                    context.Database.Migrate();
                    break;
            }

            return @this;
        }
    }
}