﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync(
            CancellationToken cancellationToken = default,
            bool asNoTracking = false );

        Task<T> GetByIdAsync( Guid id );
        
        Task<List<T>> GetByIdsAsync(ICollection<Guid> ids);
        
        Task AddAsync(T entity);
        
        Task AddRangeAsync(ICollection<T> entities);

        Task UpdateAsync(T entity);
        
        Task UpdateRangeAsync(ICollection<T> entities);

        Task DeleteAsync(T entity);
        
        Task DeleteRangeAsync(ICollection<T> entities);
    }
}