using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IEmployeeRepository: IRepository<Employee>
    {
        Task<Employee> GetByFullNameAsync( string fullName );
    }
}