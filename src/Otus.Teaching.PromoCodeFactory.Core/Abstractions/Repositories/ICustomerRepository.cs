using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface ICustomerRepository: IRepository<Customer>
    {
        Task<List<Customer>> GetByPreferenceIdsAsync( ICollection<Guid> ids );
        Task<List<Customer>> GetByPreferenceNameAsync( string preference );
    }
}