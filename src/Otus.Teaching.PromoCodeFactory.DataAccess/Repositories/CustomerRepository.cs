using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Base;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomerRepository : EfRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository( DbContext dataContext ) : base( dataContext )
        {
        }

        public new async Task DeleteAsync( Customer customer )
        {
            if( customer == null )
                return;

            var promoCodesToDelete = DataContext.Set<PromoCode>( )
                .Where( x => x.CustomerId == customer.Id )
                .ToList( );

            if( promoCodesToDelete.Any( ) )
                DataContext.Set<PromoCode>( ).RemoveRange( promoCodesToDelete );

            DataContext.Remove( customer );

            await DataContext.SaveChangesAsync( );
        }

        public async Task<List<Customer>> GetByPreferenceIdsAsync( ICollection<Guid> preferenceIds )
        {
            var customers = DataContext.Set<CustomerPreference>( )
                .Where( x => preferenceIds.Contains( x.PreferenceId ) )
                .Select( x => x.Customer )
                .ToListAsync( );

            return await customers;
        }

        public async Task<List<Customer>> GetByPreferenceNameAsync( string preference )
        {
            var customers = DataContext.Set<CustomerPreference>( )
                .Where( x => x.Preference != null &&
                             x.Preference.Name == preference )
                .Select( x => x.Customer )
                .ToListAsync( );

            return await customers;
        }
    }
}