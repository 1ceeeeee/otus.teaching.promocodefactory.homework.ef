using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Base;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class PreferenceRepository : EfRepository<Preference>, IPreferenceRepository
    {
        public PreferenceRepository( DbContext dataContext ) : base( dataContext )
        {
        }

        public async Task<Preference> GetByNameAsync( string name )
        {
            return await DataContext.Set<Preference>( )
                .FirstOrDefaultAsync( x => x.Name == name);
        }
    }
}