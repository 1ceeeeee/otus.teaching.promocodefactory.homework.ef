using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Extensions;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Base
{
    public class EfRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected readonly DbContext DataContext;
        private readonly DbSet<T> _entitySet;

        public EfRepository( DbContext dataContext )
        {
            DataContext = dataContext;
            _entitySet = DataContext.Set<T>( );
        }

        public async Task<IEnumerable<T>> GetAllAsync(
            CancellationToken cancellationToken = default,
            bool asNoTracking = false )
        {
            return await _entitySet
                .WithNoTracking( asNoTracking )
                .ToListAsync( cancellationToken );
        }

        public async Task<T> GetByIdAsync( Guid id )
        {
            return await _entitySet.FindAsync( id );
        }

        public async Task<List<T>> GetByIdsAsync( ICollection<Guid> ids )
        {
            return await _entitySet.Where( x => ids.Contains( x.Id ) ).ToListAsync( );
        }

        public async Task AddAsync( T entity )
        {
            await _entitySet.AddAsync( entity );

            await DataContext.SaveChangesAsync( );
        }

        public async Task AddRangeAsync( ICollection<T> entities )
        {
            await _entitySet.AddRangeAsync( entities );

            await DataContext.SaveChangesAsync( );
        }

        public async Task UpdateAsync( T entity )
        {
            DataContext.Entry( entity ).State = EntityState.Modified;
            await DataContext.SaveChangesAsync( );
        }

        public async Task UpdateRangeAsync( ICollection<T> entities )
        {
            foreach( var entity in entities )
                DataContext.Entry( entity ).State = EntityState.Modified;
            await DataContext.SaveChangesAsync( );
        }

        public async Task DeleteAsync( T entity )
        {
            DataContext.Set<T>( ).Remove( entity );
            await DataContext.SaveChangesAsync( );
        }

        public async Task DeleteRangeAsync( ICollection<T> entities )
        {
            DataContext.Set<T>( ).RemoveRange( entities );
            await DataContext.SaveChangesAsync( );
        }
    }
}