using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Base;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EmployeeRepository : EfRepository<Employee>, IEmployeeRepository
    {
        public EmployeeRepository( DbContext dataContext ) : base( dataContext )
        {
        }

        public async Task<Employee> GetByFullNameAsync( string fullName )
        {
            var nameArray = fullName.Split( ' ' );
            if( nameArray.Length < 2 || 
                (string.IsNullOrWhiteSpace( nameArray[ 0 ] ) && string.IsNullOrWhiteSpace( nameArray[ 1 ] )) )
                return null;
            
            return await DataContext.Set<Employee>( )
                .FirstOrDefaultAsync( x => x.FirstName == nameArray[ 0 ] &&
                                           x.LastName == nameArray[ 1 ] );
        }
    }
}