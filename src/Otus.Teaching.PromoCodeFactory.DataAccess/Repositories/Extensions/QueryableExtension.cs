using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Extensions
{
    public static class QueryableExtension
    {
        public static IQueryable<TEntity> WithNoTracking<TEntity>( 
            this IQueryable<TEntity> @this, 
            bool asNoTracking ) where TEntity: class
        {
            return asNoTracking ? @this.AsNoTracking( ) : @this;
        }
    }
}