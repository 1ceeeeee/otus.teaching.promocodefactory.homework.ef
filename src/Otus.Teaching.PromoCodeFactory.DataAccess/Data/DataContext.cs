using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DataContext : DbContext
    {
        public DataContext( DbContextOptions<DataContext> options )
            : base( options )
        {
        }

        /// <summary>
        /// Промокоды
        /// </summary>
        public DbSet<PromoCode> PromoCodes { get; set; }

        /// <summary>
        /// Покупатели
        /// </summary>
        public DbSet<Customer> Customers { get; set; }

        /// <summary>
        /// Предпочтения
        /// </summary>
        public DbSet<Preference> Preferences { get; set; }

        /// <summary>
        /// Роли
        /// </summary>
        public DbSet<Role> Roles { get; set; }

        /// <summary>
        /// Сотрудники
        /// </summary>
        public DbSet<Employee> Employees { get; set; }


        protected override void OnModelCreating( ModelBuilder modelBuilder )
        {
            base.OnModelCreating( modelBuilder );

            #region CustomerPreference

            modelBuilder.Entity<CustomerPreference>( )
                .HasKey( cp => new { cp.CustomerId, cp.PreferenceId } );

            modelBuilder.Entity<CustomerPreference>( )
                .HasOne( cp => cp.Customer )
                .WithMany( cp => cp.Preferences )
                .HasForeignKey( cp => cp.CustomerId );

            modelBuilder.Entity<CustomerPreference>( )
                .HasOne( cp => cp.Preference )
                .WithMany( )
                .HasForeignKey( bc => bc.PreferenceId );

            #endregion

            #region Customer

            modelBuilder.Entity<Customer>( )
                .HasMany( c => c.Promocodes )
                .WithOne( p => p.Customer )
                .HasForeignKey( p => p.CustomerId );

            modelBuilder.Entity<Customer>( )
                .Property( c => c.FirstName )
                .HasMaxLength( 250 );

            modelBuilder.Entity<Customer>( )
                .Property( c => c.LastName )
                .HasMaxLength( 250 );
            
            modelBuilder.Entity<Customer>( )
                .Property( p => p.Email )
                .HasMaxLength( 100 );
            
            modelBuilder.Entity<Customer>( )
                .Property( p => p.PhoneNumber )
                .HasMaxLength( 100 );

            #endregion

            #region Employee

            modelBuilder.Entity<Employee>( )
                .Property( p => p.FirstName )
                .HasMaxLength( 250 );

            modelBuilder.Entity<Employee>( )
                .Property( p => p.LastName )
                .HasMaxLength( 250 );
            
            modelBuilder.Entity<Employee>( )
                .Property( p => p.Email )
                .HasMaxLength( 100 );

            #endregion

            #region Role

            modelBuilder.Entity<Role>( )
                .Property( p => p.Name )
                .HasMaxLength( 250 );
            
            modelBuilder.Entity<Role>( )
                .Property( p => p.Description )
                .HasMaxLength( 1000 );

            #endregion

            #region PromoCode

            modelBuilder.Entity<PromoCode>( )
                .Property( p => p.PartnerManagerId )
                .IsRequired( false );
            
            modelBuilder.Entity<PromoCode>( )
                .Property( x => x.Code )
                .HasMaxLength( 250 );
            
            modelBuilder.Entity<PromoCode>( )
                .Property( x => x.PartnerName )
                .HasMaxLength( 250 );
            
            modelBuilder.Entity<PromoCode>( )
                .Property( x => x.ServiceInfo )
                .HasMaxLength( 250 );

            #endregion

            modelBuilder.Entity<Preference>( )
                .Property( x => x.Name )
                .HasMaxLength( 250 );
            
        }

        protected override void OnConfiguring( DbContextOptionsBuilder optionsBuilder )
        {
            optionsBuilder.LogTo( Console.WriteLine, LogLevel.Information );
        }
    }
}